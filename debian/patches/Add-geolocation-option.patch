From 6a0a5c3c23407654f448fc6e1beb2070f67ea573 Mon Sep 17 00:00:00 2001
From: Andreas Tille <tille@debian.org>
Last-Update: Tue, 24 Jan 2023 21:10:39 +0100
Forwarded: https://gitlab.com/wavexx/fgallery/-/issues/62
Subject: [PATCH] Add geolocation option
  The patch was sumitted in issue
    https://github.com/wavexx/fgallery/issues/44
  which was closed without any reason by upstream on Jan 29, 2019
  Another issue with the same topic was opened
    https://github.com/wavexx/fgallery/issues/62
  and closed without comment as well

This is a refresh of the geolocation_patches.zip

See https://github.com/wavexx/fgallery/issues/44
---
 fgallery      | 25 +++++++++++++++++++++++--
 fgallery.1    | 12 +++++++++++-
 view/index.js |  7 +++++++
 3 files changed, 41 insertions(+), 3 deletions(-)

--- a/fgallery
+++ b/fgallery
@@ -37,6 +37,9 @@ my $JSON_cls = eval { require Cpanel::JS
 	       fatal("either Cpanel::JSON::XS or JSON::PP is required");
 $JSON_cls->import(qw{encode_json});
 
+use POSIX qw(setlocale LC_NUMERIC);
+setlocale( LC_NUMERIC, "C" );
+
 # constants
 our $VERSION = "1.9.1";
 our $ENCODING = encoding::_get_locale_encoding() || 'UTF-8';
@@ -76,6 +79,13 @@ my $verbose = 0;
 my $workers = 0;
 my $sRGB = 1;
 my $indexUrl = undef;
+my $dogeo = 0;
+my $geobaseUrl = "http://osm.org";
+my $geoZoom = 17;
+# This format string works with OSM
+# In case this should be adapted to other map providers a more
+# sophisticated format parsing than just sprintf() would be needed
+my $defaultgeoformat = "%s?zoom=%i&lat=%-10.9g&lon=%-10.9g";
 my @capmethods = ("txt", "xmp", "exif");
 
 
@@ -365,6 +375,7 @@ sub print_help
   -p			do not automatically include full-sized panoramas
   -d			do not generate a full album download
   -f			improve thumbnail cutting by performing face detection
+  -g                    respect available GPS coordinates in images
   -j N			set process-level parallelism
   --max-full WxH	maximum full image size ($maxfull[0]x$maxfull[1])
   --max-thumb WxH	maximum thumbnail size ($maxthumb[0]x$maxthumb[1])
@@ -372,6 +383,8 @@ sub print_help
   --no-sRGB		do not remap preview/thumbnail color profiles to sRGB
   --quality Q		preview image quality (0-100, currently: $imgq)
   --index url		specify the URL location for the index/back button
+  --geobase url		specify the URL location geolocation (default: $geobaseUrl)
+  --geozoom z		specify the zoom level of geo maps (default: $geoZoom)
 });
   exit(shift);
 }
@@ -385,6 +398,7 @@ my ($ret, @ARGS) = GetOptions(
   'c=s' => sub { @capmethods = parse_cap($_[0], $_[1]); },
   'd' => sub { $nodown = 1; },
   'f' => sub { $facedet = 1; },
+  'g' => sub { $dogeo = 1; },
   'i' => sub { $ofile = 1; },
   'j=i' => sub { $workers = parse_int($_[0], $_[1], 1, undef); },
   'o' => sub { $orient = 0; },
@@ -399,7 +413,9 @@ my ($ret, @ARGS) = GetOptions(
   'min-thumb=s' => sub { @minthumb = parse_wh(@_); },
   'no-sRGB' => sub { $sRGB = 0; },
   'quality=i' => sub { $imgq = parse_int($_[0], $_[1], 0, 100); },
-  'index=s' => sub { $indexUrl = $_[1]; });
+  'index=s' => sub { $indexUrl = $_[1]; },
+  'geobase=s' => sub { $geobaseUrl = $_[1]; $dogeo = 1; },
+  'geozoom=i' => sub { $geoZoom = parse_int($_[0], $_[1], 0, 19); $dogeo = 1; });
 
 if(@ARGV < 2 || @ARGV > 3 || !$ret) {
   print_help(2);
@@ -559,6 +575,13 @@ sub analyze_file
     $props->{'OrigImageWidth'} = $props->{"ExifImageWidth ($n)"};
     $props->{'OrigImageHeight'} = $props->{"ExifImageHeight ($n)"};
   }
+  if($dogeo) {
+    my $mlat = $props->{"GPSLatitude"};
+    my $mlon = $props->{"GPSLongitude"};
+    if ( $mlat && $mlon ) {
+      $props->{'geourl'} = sprintf($defaultgeoformat, $geobaseUrl, $geoZoom, $mlat, $mlon);
+    }
+  }
 
   # extract caption
   foreach my $m(@capmethods)
@@ -906,7 +929,7 @@ foreach my $fdata(@adata)
       $data{$_} = $fdata->{$_};
     }
   }
-  foreach('date', 'stamp', 'caption') {
+  foreach('date', 'stamp', 'caption', 'geourl') {
     if(defined($fdata->{props}{$_})) {
       $data{$_} = $fdata->{props}{$_};
     }
--- a/fgallery.1
+++ b/fgallery.1
@@ -6,7 +6,7 @@
 .Nd static HTML+JavaScript photo album generator
 .Sh SYNOPSIS
 .Nm
-.Op Fl hvsioktrpdf
+.Op Fl hvsioktrpdfg
 .Op Fl c Ar methods
 .Op Fl j Ar N
 .Op Fl -max-full Ar WxH
@@ -15,6 +15,8 @@
 .Op Fl -no-sRGB
 .Op Fl -quality Ar Q
 .Op Fl -index Ar URL
+.Op Fl -geobase Ar URL
+.Op Fl -geozoom Ar z
 .Ar input-dir
 .Ar output-dir
 .Op Ar album name
@@ -105,6 +107,8 @@ Improve thumbnail cutting by performing
 Requires
 .Xr facedetect 1
 to be installed.
+.It Fl g
+Respect GPS cooredinates of images
 .It Fl c Ar methods
 Set the caption extraction method (txt, xmp, exif, cmt or none).
 Multiple methods can be specified by giving a comma-separated list.
@@ -133,6 +137,12 @@ Set the JPEG quality of the preview imag
 The default is 90.
 .It Fl -index Ar URL
 Specify the URL location for the index and back buttons.
+.It Fl -geobase Ar url
+Specify the URL location geolocation (default: http://osm.org)
+(when --geobase is specified -g is implicitly set)
+.It Fl -geozoom Ar z
+Specify the zoom level of geo maps (default: 17)
+(when --geozoom is specified -g is implicitly set)
 .El
 .Sh SEE ALSO
 .Xr ImageMagick 1 ,
--- a/view/index.js
+++ b/view/index.js
@@ -445,6 +445,13 @@ function setupHeader()
     el.set('html', '<img src=\"eye.png\"/>');
     ehdr.adopt(el);
   }
+  if(imgs.data[eidx].geourl)
+  {
+    var geourl = imgs.data[eidx].geourl;
+    var el = new Element('a', { 'target': 'map', 'title': 'Show map position', 'href': geourl });
+    el.set('html', '<img src=\"map.png\"/>');
+    ehdr.adopt(el);
+  }
   if(imgs.download)
   {
     var el = new Element('a', { 'title': 'Download album', 'href': imgs.download });
