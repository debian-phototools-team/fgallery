Source: fgallery
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Daniel Lenharo de Souza <lenharo@debian.org>,
           Andreas Tille <tille@debian.org>
Section: web
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/fgallery
Vcs-Git: https://salsa.debian.org/debian-phototools-team/fgallery.git
Homepage: https://www.thregr.org/~wavexx/software/fgallery/
Rules-Requires-Root: no

Package: fgallery
Architecture: all
Depends: ${misc:Depends},
         python3,
         libimage-exiftool-perl,
         libjson-perl,
         imagemagick,
         exiftran | libjpeg-turbo-progs,
         libjs-mootools,
         7zip | zip
Recommends: liblcms2-utils,
            facedetect
Suggests: jpegoptim,
          pngcrush
Description: static HTML+JavaScript photo album generator
 “fgallery” is a static photo gallery generator with no frills that has a
 stylish, minimalist look. “fgallery” shows your photos, and nothing else.
 .
 There is no server-side processing, only static generation. The resulting
 gallery can be uploaded anywhere without additional requirements and works with
 any modern browser.
 .
  * Automatically orients pictures without quality loss.
  * Multi-camera friendly: automatically sorts pictures by time: just throw your
    (and your friends) photos and movies in a directory. The resulting gallery
    shows the pictures in seamless shooting order.
  * Adapts to the current screen size and proportions, switching from
    horizontal/vertical layout and scaling thumbnails automatically.
  * Includes original (raw) pictures in a zip file for downloading.
  * Panoramas can be seen full-size by default.
